---
object-id: windowless-weather
name: Windowless Weather
image: images/windowless-weather.jpg
link: https://dapierce.github.io/my-freecodecamp/windowless-weather/
listing-priority: 5
---

Simple weather web app that polls your local time and location and gives a pleasant display of the current weather with time-of-day specific colors.

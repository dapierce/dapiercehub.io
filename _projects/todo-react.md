---
object-id: todo-react
name: Todo App
image: images/todo-react.png
link: http://rocky-gorge-23758.herokuapp.com/
listing-priority: 2
---

A simple todo app, created in React. Currently uses local storage to maintain data.
